<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="messages")
 * @ORM\Entity()
 */
class Message
{
    /**
     * @var int
     *
     * @JMS\Groups({"post", "get"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @JMS\Groups({"post", "get"})
     *
     * @ORM\Column(name="msg", type="string", length=200)
     */
    private $messageText;

    /**
     * @var Message
     *
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="childMessages")
     * @ORM\JoinColumn(name="parent_message_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parentMessage;

    /**
     * @var ArrayCollection|Message[]
     *
     * @JMS\Groups({"get"})
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="parentMessage")
     */
    private $childMessages;

    /**
     * @var \DateTime $createdAt
     *
     * @JMS\Groups({"post", "get"})
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct() {
        $this->childMessages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessageText(): ?string
    {
        return $this->messageText;
    }

    /**
     * @param string $messageText
     * @return $this
     */
    public function setMessageText(string $messageText)
    {
        $this->messageText = $messageText;
        return $this;
    }

    /**
     * @return Message
     */
    public function getParentMessage(): ?Message
    {
        return $this->parentMessage;
    }

    /**
     * @param Message $parentMessage
     * @return $this
     */
    public function setParentMessage(Message $parentMessage)
    {
        $this->parentMessage = $parentMessage;
        return $this;
    }

    /**
     * @return Message[]|ArrayCollection
     */
    public function getChildMessages()
    {
        return $this->childMessages;
    }

    /**
     * @param Message[]|ArrayCollection $childMessages
     * @return $this
     */
    public function setChildMessages($childMessages)
    {
        $this->childMessages = $childMessages;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function __toString()
    {
        return (string) $this->messageText;
    }
}
