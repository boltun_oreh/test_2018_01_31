<?php

namespace App\Api\Controller;

use App\Entity\Message;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class ChatController extends FOSRestController
{
    /**
     * @Rest\Post("/message")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the message tree",
     *     @Model(type=Message::class)
     * )
     * @SWG\Parameter(
     *     name="message",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="message_text", type="string"),
     *         @SWG\Property(property="parent_id", type="integer")
     *     )
     * )
     * @SWG\Tag(name="message")
     */
    public function postMessage(Request $request)
    {
        $message = new Message();

        $messageText = $request->get('message_text');
        $parentMessageId = $request->get('parent_id');

        // check if $messageText is empty, but not 0 or "0"
        if (null === $messageText || '' === $messageText) {
            $view = $this->view([
                'error_message' => 'Message text can not be empty',
            ], Response::HTTP_BAD_REQUEST);

            return $this->handleView($view);
        }

        if (null != $parentMessageId) {
            $parentMessage = $this->getDoctrine()->getRepository(Message::class)->find($parentMessageId);

            if (null == $parentMessage) {
                $view = $this->view([
                    'error_message' => 'Parrent message not found',
                ], Response::HTTP_NOT_FOUND);

                return $this->handleView($view);
            }

            $message->setParentMessage($parentMessage);
        }

        $message->setMessageText($messageText);

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        $view = $this->view($message, Response::HTTP_CREATED);
        $context = new Context();
        $context->setGroups(['post']);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/tree/{message}")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the message tree",
     *     @Model(type=Message::class)
     * )
     * @SWG\Parameter(
     *     name="message",
     *     in="path",
     *     type="integer"
     * )
     * @SWG\Tag(name="message")
     */
    public function getMessageTree(Request $request)
    {
        // using of @ParamConverter looks more elegant but it leads to 404 exception, when Message is not found
        $messageId = $request->get('message');
        $message = $this->getDoctrine()->getRepository(Message::class)->find($messageId);

        if (null == $message) {
            $view = $this->view([
                'error_message' => 'Message not found',
            ], Response::HTTP_NOT_FOUND);

            return $this->handleView($view);
        }

        $view = $this->view($message, Response::HTTP_OK);
        $context = new Context();
        $context->setGroups(['get']);
        $view->setContext($context);

        return $this->handleView($view);
    }
}
