<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChatControllerTest extends WebTestCase
{
    public function postDataProvider()
    {
        return [
            ['test message', 0, 201],
            ['test message', null, 201],
            ['test message', 999, 404],
            [0, null, 201],
            ['0', null, 201],
            ['null', null, 201],
            [null, null, 400],
            ['', null, 400],
        ];
    }

    /**
     * @dataProvider postDataProvider
     */
    public function testPostMessage($message, $parent, $expected)
    {
        $client = static::createClient();

        $client->request('POST', '/api/message', [
            'message_text' => $message,
            'parent_id' => $parent,
        ]);

        $this->assertEquals($expected, $client->getResponse()->getStatusCode());
    }
}
